import 'package:flutter/material.dart';
import 'reg_main.dart';

void main() {
  runApp(const LoginPage());
}

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          children: [
            const Spacer(flex: 5),
            Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png",
              height: 200,
              width: 200,
            ),
            const Spacer(flex: 10),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            const Spacer(flex: 3),
            ElevatedButton(onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Reg_mainApp()),
              );
            }, child: const Text('Login')),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
