import 'package:flutter/material.dart';

void main() {
  runApp(Reg_mainApp());
}

class MyAppTheme {
  static ThemeData appTheme() {
    return ThemeData(
      brightness: Brightness.light,
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    );
  }
}

class Reg_mainApp extends StatefulWidget {
  @override
  State<Reg_mainApp> createState() => _Reg_mainAppState();
}

class _Reg_mainAppState extends State<Reg_mainApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        title: 'REG BUU',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Reg Burapha University',
                style: TextStyle(color: Colors.black)),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.accessible_forward),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          body: Container(
            child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                mainPage(),
                                timeTable(),
                                announce(),
                                info(),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            // height: 200,
                            child: Image.network(
                              "https://cdn.discordapp.com/attachments/1073510481542774794/1073510608080744509/peem.jpg",
                              height: 175,
                              width: 175,
                            ),
                          ),
                          Container(
                            child: Text("ประวัติส่วนตัว",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 30)),
                          ),
                          Container(
                            child: Text("ชื่อ ศิวกร   จันทร์เล็ก ",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                )),
                          ),
                          Container(
                            child: Text("ชื่ออังกฤษ Siwakorn   Janlek ",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("คณะ วิทยาการสารสนเทศ",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("สาขา วิทยาการคอมพิวเตอร์",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("รหัสนิสิต 631160013",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("ระดับการศึกษา ปริญญาตรี",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("อาจารย์ที่ปรึกษา",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            child: Text("อาจารย์ภูสิต กุลเกษม",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            // height: 200,
                            child: Image.network(
                              "https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png",
                              height: 125,
                              width: 125,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }

  Widget mainPage() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.account_circle,
            color: Colors.grey,
          ),
          onPressed: () {},
        ),
        Text("Home", style: TextStyle(color: Colors.black, fontSize: 15)),
      ],
    );
  }

  Widget timeTable() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.add_card,
            color: Colors.grey,
          ),
          onPressed: () {
            AlertDialog(
                  title: const Text('AlertDialog Title'),
                  content: const Text('AlertDialog description'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: const Text('Cancel'),
                    ),
                  ],
                );
          },
        ),
        Text("Class", style: TextStyle(color: Colors.black, fontSize: 15)),
      ],
    );
  }

  Widget announce() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.account_balance_rounded,
            color: Colors.grey,
          ),
          onPressed: () {},
        ),
        Text("announce", style: TextStyle(color: Colors.black, fontSize: 15)),
      ],
    );
  }

  Widget info() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.account_balance_wallet,
            color: Colors.grey,
          ),
          onPressed: () {},
        ),
        Text("info", style: TextStyle(color: Colors.black, fontSize: 15)),
      ],
    );
  }
}
