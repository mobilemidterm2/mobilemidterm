
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'login_page.dart';
import 'reg_main.dart';

void main() {
  runApp(Reg());
}

class Reg extends StatelessWidget {
  const Reg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG BUU',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Reg Burapha University',style: TextStyle(color: Colors.black)),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.accessible_forward),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          body: LoginPage(),
          ),
        ),
      );
  }
}
